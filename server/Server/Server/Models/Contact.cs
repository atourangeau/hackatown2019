﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Models
{
  public class Contact
  {
    public int Id { get; set; }

    public string Name { get; set; }
  }
}