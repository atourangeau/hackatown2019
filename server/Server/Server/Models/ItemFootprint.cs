﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Models
{
  public enum ItemType
  {
    // Par 1h pour emission, par année pour le reste
    Transport_HeavyGaz=0,
    Transport_LightGaz,
    Transport_Hybrid,
    Transport_Electric,
    Transport_Shared,
    Transport_CommuteGas,
    Transport_CommuteElec,
    Transport_Manual,
    Transport_Plane,

    // Par morceau
    Fabric_Cotton =10,
    Fabric_CottonOrganic,
    Fabric_Lin,
    Fabric_Polyester,
    Fabric_Leather,
    Fabric_Jean,

    // Par Kg de chacun
    // Note: 1portion ~ 250g. Si l'API se base sur 1 portion, on doit juste tout diviser en 4.
    Food_WhiteMeat =20, //Oiseaux+Porc
    Food_RedMeatH,  //Agneau/Boeuf
    Food_RedMeatL,  //Autre rouge
    Food_Fish,      //Poisson
    Food_Dry,       //Cannes, Pâtes, Etc
    Food_Other,     //Légumes, Fruits, Légumineuses, Céréales
    Food_OtherBio,  //Same + Bio

    // Par 100pi2 par année
    Housing_Space=30,
    // Par 100$ d'hydro
    Housing_Energy, //TODO: Support all energy types. For now, assuming "Hydro"

    // Par objet
    Object_Phone =40,
    Object_Desktop,
    Object_Laptop,
    Object_Monitor,
    Object_Recyclable,
    Object_Trash,
  }

  public class ItemFootprint : Footprint
  {
    public ItemType Type { get; set; }
  }
}