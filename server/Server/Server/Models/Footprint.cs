﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server.Models
{
  public class Footprint
  {
    //Basé sur https://www.skepticalscience.com/human-co2-smaller-than-natural-emissions.htm, la forêt nous laisse un delta de -11 gigaton de CO2.
    //11 000 000 000 000 kg -> 8 milliard humains ==> Max théorique de 1375 par humain
    const int MAX_EMISSION = 1375;
    //Selon https://water.usgs.gov/edu/earthhowmuch.html
    //Et le fait que l'eau refresh environ au 2 semaines dans les lacs/rivières
    const int MAX_WATER = (91000+2120+1120)*52/2;
    //Fkn arbitraire parce que j'ai aucun knowledge :D (En ce moment le Can fait ~370/habitant et on sait qu'il consomme 9 planettes par années, so on va viser 40).
    const int MAX_WASTE = 40;
    //Total habitable 134 000 000 km2 -> 8 milliard humains ==> Max théorique de 16 750m2 par humain
    const int MAX_LAND = 16750;

    public float Emission { get; set; } //En Kg de C02
    public float Water { get; set; }    //En Litres
    public float Waste { get; set; }    //En Kg sur un an (ce qui ce composte ne compte donc pas, ce qui se désintègre sur 10 ans peut être amorti sur 10 ans, le plastique compte en full)
    public float Land { get; set; }     //En Mètre2
  }
}