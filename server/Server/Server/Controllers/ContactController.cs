﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Server.Models;
using Server.Repositories;

namespace Server.Controllers
{
  public class ContactController : ApiController
  {
    private IContactRepository contactRepository;

    public ContactController(IContactRepository contactRepository)
    {
      this.contactRepository = contactRepository;
    }

    public Contact[] Get()
    {
      return contactRepository.GetAllContacts();
    }

    public HttpResponseMessage Post(Contact contact)
    {
      this.contactRepository.SaveContact(contact);

      var response = Request.CreateResponse<Contact>(System.Net.HttpStatusCode.Created, contact);

      return response;
    }
  }
}
