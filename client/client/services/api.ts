import AirQualityResponse from "../models/air-quality-response";
import Station from "../models/station";
import Polluant from "../models/polluant";
import AirQualityContainer from "../models/air-quality-container";
import AirQuality, { AirStatusEnum } from "../models/air-quality";

const parser = require("react-native-xml2js").parseString;
var XMLParser = require("react-xml-parser");

export function getAirQuality(): Promise<AirQuality> {
  return fetch("http://ville.montreal.qc.ca/rsqa/servlet/makeXmlActuel")
    .then(response => response.text())
    .then(response => {
      return executeParsing(response);
    })
    .catch(err => {
      console.log(err);
      return { value: 0, status: AirStatusEnum.Unknown };
    });
}

function executeParsing(response: string) {
  let parser = new XMLParser();
  var xml = parser.parseFromString(response);

  let stations = xml.getElementsByTagName("echantillon");

  var date = new Date();
  let hour = date.getHours();

  let i = 0;
  let nbOfOccurence = 0;
  let current = stations[i];
  let sum = 0;
  while (current !== undefined) {
    let attribute = stations[i].attributes;
    if (attribute !== undefined && Number(attribute.heure) === hour - 2) {
      console.log(attribute.heure);
      let co2 = stations[i].children[1].attributes.value;
      sum = sum + Number(co2);
      nbOfOccurence++;
    }
    ++i;
    current = stations[i];
  }

  let pollution = sum / nbOfOccurence;

  pollution = (pollution / 35) * 50;

  console.log(pollution);

  let result = {
    value: pollution,
    status: AirStatusEnum.Good
  };

  if (pollution <= 25) result.status = AirStatusEnum.Good;
  if (pollution > 25 && pollution <= 50)
    result.status = AirStatusEnum.Acceptable;
  if (pollution > 50) result.status = AirStatusEnum.Bad;

  return result;
}
