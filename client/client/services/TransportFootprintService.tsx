import {ItemFootprint} from './FootprintService';
import {getItemFootprint} from './FootprintService';


export function getTransportFootprint (itemType: string, numberOfDays: number, hoursPerDay: number) : ItemFootprint|undefined {
    let itemFootprint = getItemFootprint(itemType);
    if (itemFootprint) {
        itemFootprint.Emission = itemFootprint.Emission * numberOfDays * hoursPerDay;
        //No need to change the other types of impact. They are annual and don't change based on usage
    }
    return itemFootprint;
}
