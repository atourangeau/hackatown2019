import {ItemFootprint} from './FootprintService';
import {getItemFootprint} from './FootprintService';

export function getHousingSizeFootprint (sizeInSquareFt: number) : ItemFootprint|undefined {
    let itemFootprint = getItemFootprint("Housing_Space");
    if (itemFootprint) {
        itemFootprint.Emission = itemFootprint.Emission * sizeInSquareFt / 100;
        itemFootprint.Water = itemFootprint.Water * sizeInSquareFt / 100;
        itemFootprint.Waste = itemFootprint.Waste * sizeInSquareFt / 100;
        itemFootprint.Land = itemFootprint.Land * sizeInSquareFt / 100;
    }
    return itemFootprint;
}

export function getHousingEnergyFootprint (annualCost: number) : ItemFootprint|undefined {
    let itemFootprint = getItemFootprint("Housing_Energy");
    if (itemFootprint) {
        itemFootprint.Emission = itemFootprint.Emission * annualCost / 100;
        itemFootprint.Water = itemFootprint.Water * annualCost / 100;
        itemFootprint.Waste = itemFootprint.Waste * annualCost / 100;
        itemFootprint.Land = itemFootprint.Land * annualCost / 100;
    }
    return itemFootprint;
}