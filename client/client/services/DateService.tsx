export function getNumberOfDaysSinceStartOfYEar() : number {
    let currentDate = new Date();
    let month = currentDate.getMonth();
    let day = currentDate.getDate();

    return month*30+day;
}