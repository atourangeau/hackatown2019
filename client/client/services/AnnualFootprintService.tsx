import {Footprint} from './FootprintService';
import {ItemFootprint} from './FootprintService';

//Basé sur https://www.skepticalscience.com/human-co2-smaller-than-natural-emissions.htm, la forêt nous laisse un delta de -11 gigaton de CO2.
//11 000 000 000 000 kg -> 8 milliard humains ==> Max théorique de 1375 par humain
//For some reason, le tour d'avion me donne un impact 4 fois plus grand que quand jle faisais sur un autre site... on va se laisser plus de marge
const MAX_EMISSION = 1375*4;
//Selon https://water.usgs.gov/edu/earthhowmuch.html
//Et le fait que l'eau refresh environ au 2 semaines dans les lacs/rivières
const MAX_WATER = (91000+2120+1120)*52/2;
//Fkn arbitraire parce que j'ai aucun knowledge :D (En ce moment le Can fait ~370/habitant et on sait qu'il consomme 9 planettes par années, so on va viser 80).
const MAX_WASTE = 80;
//Total habitable 134 000 000 km2 -> 8 milliard humains ==> Max théorique de 16 750m2 par humain
const MAX_LAND = 16750;

export function getNumberOfEarths(items: ItemFootprint[]) : number {
    let totalFootprint = getTotalFootprint(items);
    return parseFloat(((totalFootprint.Emission/MAX_EMISSION + totalFootprint.Water/MAX_WATER
    + totalFootprint.Waste/MAX_WASTE + totalFootprint.Land/MAX_LAND)/4).toFixed(3));
}

export function getDetailedNumberOfEarths(items: ItemFootprint[]) : Footprint {
    let totalFootprint = getTotalFootprint(items);
    totalFootprint.Emission = parseFloat((totalFootprint.Emission/MAX_EMISSION).toFixed(2));
    totalFootprint.Water = parseFloat((totalFootprint.Water/MAX_WATER).toFixed(2));
    totalFootprint.Waste = parseFloat((totalFootprint.Waste/MAX_WASTE).toFixed(2));
    totalFootprint.Land = parseFloat((totalFootprint.Land/MAX_LAND).toFixed(2));
    return totalFootprint;
}

function getTotalFootprint(items: ItemFootprint[]) : Footprint {
    let totalFootprint = {Emission: 0, Water: 0, Waste: 0, Land: 0};
    items.forEach(function (item) {
        totalFootprint = addItemToTotalFootprint(totalFootprint, item);
    });
    return totalFootprint;
}

export interface recommendation {
    ItemType: string,
    AnnualPercentage: number
}

export function getRecommendation(items: ItemFootprint[]) : Map<string, recommendation> {
    let recommendations = new Map<string, recommendation>();
    recommendations.set("Emission", {ItemType: "None", AnnualPercentage: 0});
    recommendations.set("Water", {ItemType: "None", AnnualPercentage: 0});
    recommendations.set("Waste", {ItemType: "None", AnnualPercentage: 0});
    recommendations.set("Land", {ItemType: "None", AnnualPercentage: 0});
    let totalFootprint = {Emission: 0, Water: 0, Waste: 0, Land: 0};
    let subtotalFootprints = new Map<string, Footprint>();

    items.forEach(function (item) {
        totalFootprint = addItemToTotalFootprint(totalFootprint, item);
        if (item.ItemType.includes("Fabric")) {
            subtotalFootprints.set("Fabric", addItemToTotalFootprint(subtotalFootprints.get("Fabric"), item));
        }
        else if (item.ItemType.includes("Food")) {
            subtotalFootprints.set("Food", addItemToTotalFootprint(subtotalFootprints.get("Food"), item));
        }
        else if (item.ItemType.includes("Housing")) {
            subtotalFootprints.set("Housing", addItemToTotalFootprint(subtotalFootprints.get("Housing"), item));
        }
        else if (item.ItemType.includes("Object")) {
            subtotalFootprints.set("Object", addItemToTotalFootprint(subtotalFootprints.get("Object"), item));
        }
        else if (item.ItemType.includes("Transport")) {
            subtotalFootprints.set("Transport", addItemToTotalFootprint(subtotalFootprints.get("Transport"), item));
        }
        else {
            console.log("ERROR: Invalid item received and can't be calculated: " + item);
        }
    });

    subtotalFootprints.forEach(function (subFootprint, key) {
        let emissionPercentage = parseFloat((subFootprint.Emission/totalFootprint.Emission*100).toFixed(1));
        let emissionRecommendation = recommendations.get("Emission");
        if (emissionRecommendation && emissionPercentage > emissionRecommendation.AnnualPercentage) {
            emissionRecommendation.ItemType = key;
            emissionRecommendation.AnnualPercentage = emissionPercentage;
            recommendations.set("Emission", emissionRecommendation);
        }

        let waterPercentage = parseFloat((subFootprint.Water/totalFootprint.Water*100).toFixed(1));
        let waterRecommendation = recommendations.get("Water");
        if (waterRecommendation && waterPercentage > waterRecommendation.AnnualPercentage) {
            waterRecommendation.ItemType = key;
            waterRecommendation.AnnualPercentage = waterPercentage;
            recommendations.set("Water", waterRecommendation);
        }

        let wastePercentage = parseFloat((subFootprint.Waste/totalFootprint.Waste*100).toFixed(1));
        let wasteRecommendation = recommendations.get("Waste");
        if (wasteRecommendation && wastePercentage > wasteRecommendation.AnnualPercentage) {
            wasteRecommendation.ItemType = key;
            wasteRecommendation.AnnualPercentage = wastePercentage;
            recommendations.set("Waste", wasteRecommendation);
        }

        let landPercentage = parseFloat((subFootprint.Land/totalFootprint.Land*100).toFixed(1));
        let landRecommendation = recommendations.get("Land");
        if (landRecommendation && landPercentage > landRecommendation.AnnualPercentage) {
            landRecommendation.ItemType = key;
            landRecommendation.AnnualPercentage = landPercentage;
            recommendations.set("Land", landRecommendation);
        }
    });

    return recommendations;
}

function addItemToTotalFootprint(totalFootprint: Footprint|undefined, item: ItemFootprint) : Footprint {
    if (!totalFootprint) return {Emission: item.Emission, Water: item.Water, Waste: item.Waste, Land: item.Land};
    totalFootprint.Emission += item.Emission;
    totalFootprint.Water += item.Water;
    totalFootprint.Waste += item.Waste;
    totalFootprint.Land += item.Land;
    return totalFootprint;
}