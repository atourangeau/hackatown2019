const jsonData = require("../data/BaseEcoData.json");

export interface ItemFootprint extends Footprint{
    ItemType: string
}

export interface Footprint {
    Emission: number,
    Water: number,
    Waste: number,
    Land: number
}

const data : ItemFootprint[] = jsonData;

export function getItemFootprint (itemType: string) : ItemFootprint|undefined {
    let itemFootprint = data.find(itemFootprint => itemFootprint.ItemType === itemType);
    if (!itemFootprint) {
        console.log("Unknown object with type " + itemType);
        return itemFootprint;
    }
    // Need to copy the object in case we modify the values (don't want to modify base values).
    return {
        ItemType: itemFootprint.ItemType,
        Emission: itemFootprint.Emission,
        Water: itemFootprint.Water,
        Waste: itemFootprint.Waste,
        Land: itemFootprint.Land
    };
}

/**
 * Pour les items comestibles
 */
export function getItemFootprintByPortion (itemType: string, portion: number) {
    let itemFootprint = getItemFootprint(itemType);
    if (itemFootprint) {
        itemFootprint.Emission = itemFootprint.Emission * portion/4;
        itemFootprint.Water = itemFootprint.Water * portion/4;
        itemFootprint.Waste = itemFootprint.Waste * portion/4;
        itemFootprint.Land = itemFootprint.Land * portion/4;
    }
    return itemFootprint;
}

/**
 * Pour les items auquels on peut attribuer un compte
 * Ex: Objets/Vêtements/Meubles/Électros
 */
export function getItemFootprintByOccurence (itemType: string, occurence: number) {
    let itemFootprint = getItemFootprint(itemType);
    if (itemFootprint) {
        itemFootprint.Emission = itemFootprint.Emission * occurence;
        itemFootprint.Water = itemFootprint.Water * occurence;
        itemFootprint.Waste = itemFootprint.Waste * occurence;
        itemFootprint.Land = itemFootprint.Land * occurence;
    }
    return itemFootprint;
}
