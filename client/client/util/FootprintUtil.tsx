import {Footprint, ItemFootprint} from '../services/FootprintService';

export function addFootprints(footprint1: ItemFootprint, footprint2: ItemFootprint) : Footprint {
    return {
        Emission: footprint1.Emission + footprint2.Emission,
        Water: footprint1.Water + footprint2.Water,
        Waste: footprint1.Waste + footprint2.Waste,
        Land: footprint1.Land + footprint2.Land,
    }
}

export function multiplyFootprints(footprint: ItemFootprint, mult: number) : ItemFootprint {
    return {
        ItemType: footprint.ItemType,
        Emission: footprint.Emission * mult,
        Water: footprint.Water * mult,
        Waste: footprint.Waste * mult,
        Land: footprint.Land * mult,
    }
}