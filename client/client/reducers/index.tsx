import { EnthusiasmAction } from "../actions/index";
import { StoreState } from "../types/index";
import {
  INCREMENT_ENTHUSIASM,
  DECREMENT_ENTHUSIASM,
  ADD_CLOTHE,
  ADD_ELECTRONIC,
  ADD_FOOD,
  ADD_USUAL_OBJECTS,
  GET_FOOTPRINT,
  CONFIGURE_HABITATION,
  CONFIGURE_TRANSPORT,
  GET_AIR_QUALITY,
  ADD_TRANSPORT

} from "../constants/index";
import { entryStore } from "../data/entry-store";
import {
  getItemFootprintByOccurence,
  getItemFootprintByPortion,
  Footprint,
  ItemFootprint
} from "../services/FootprintService";
import { getNumberOfDaysSinceStartOfYEar } from "../services/DateService";
import {
  getHousingEnergyFootprint,
  getHousingSizeFootprint
} from "../services/HousingFootprintService";
import { getTransportFootprint } from "../services/TransportFootprintService";
import {
  getNumberOfEarths,
  getDetailedNumberOfEarths,
  getRecommendation,
  recommendation
} from "../services/AnnualFootprintService";
import { getAirQuality } from "../services/api";
import AirQuality, { AirStatusEnum } from "../models/air-quality";
import {multiplyFootprints} from "../util/FootprintUtil";

export const initialState: StoreState = {
  enthusiasmLevel: 1,
  languageName: "Typescript",
  earthsNumber: 0,
  details: {
    Emission: 0,
    Water: 0,
    Waste: 0,
    Land: 0
  },
  recommandations: new Map<string, recommendation>(),
  airQuality: {
    value: 0,
    status: AirStatusEnum.Unknown
  }
};

export function earthReducer(
  state: StoreState = initialState,
  action: EnthusiasmAction
): StoreState {
  console.log("TEST")
  switch (action.type) {
    case ADD_CLOTHE: {
      let footprint = getItemFootprintByOccurence(
        action.clothe.clotheType,
        action.clothe.occurence
      );

      if (footprint !== undefined) {
          if (action.clothe.isBio) {
              multiplyFootprints(footprint, 0.5);
          }
          if (action.clothe.isReused) {
              multiplyFootprints(footprint, 0.25);
          }
        entryStore.entries.push(footprint);
      }

      return getUpdatedState(state);
    }
    case ADD_ELECTRONIC: {
      let footprint = getItemFootprintByOccurence(
        action.electronic.electronicType,
        action.electronic.occurence
      );

      if (footprint !== undefined) {
        entryStore.entries.push(footprint);
      }

      return getUpdatedState(state);
    }
    case ADD_FOOD: {
      let footprint = getItemFootprintByPortion(
        action.food.foodType,
        action.food.portion
      );

      if (footprint !== undefined) {
          if (action.food.isBio) {
              multiplyFootprints(footprint, 0.5);
          }
        entryStore.entries.push(footprint);
      }

      return getUpdatedState(state);
    }
    case ADD_USUAL_OBJECTS: {
       let footprint = getItemFootprintByOccurence(action.usualObjects.objectType, action.usualObjects.occurence);
       if (footprint !== undefined) {
          if( action.usualObjects.isReused){
            multiplyFootprints(footprint, 0.5);

          }
          entryStore.entries.push(footprint);
       }
       return getUpdatedState(state);
      return state;
    }
    case ADD_TRANSPORT: {
        let footprint = getTransportFootprint(
            action.transport.transportType,
            action.transport.daysPerWeek,
            action.transport.hoursOfUse
        );

        if (footprint) {
            entryStore.entries.push(footprint);
        }

        return getUpdatedState(state);
    }
    case CONFIGURE_HABITATION: {
      let nbDay = getNumberOfDaysSinceStartOfYEar();
      let sizeFootprint = getHousingSizeFootprint(
        action.habitation.sizeInSquareFt
      );
      let energyFootprint = getHousingEnergyFootprint(
        action.habitation.annualEnergyCost
      );

      if (sizeFootprint !== undefined) {
        multiplyFootprints(sizeFootprint, nbDay/(365*action.habitation.numberOfHabitants));
        entryStore.houseSizeConfig = sizeFootprint;
      }
      if (energyFootprint !== undefined) {
        multiplyFootprints(energyFootprint, nbDay/(365*action.habitation.numberOfHabitants));
        entryStore.houseEnergyConfig = energyFootprint;
      }

      return getUpdatedState(state);
    }
    case CONFIGURE_TRANSPORT: {
      let nbDay = getNumberOfDaysSinceStartOfYEar();
      let transportFootprint = getTransportFootprint(
        action.transport.transportType,
        action.transport.daysPerWeek,
        action.transport.hoursOfUse
      );

      if (transportFootprint !== undefined) {
        // Only scale emission based on number of weeks, since the rest is already calculated annualy.
        transportFootprint.Emission = transportFootprint.Emission * nbDay/7;
        entryStore.transportConfig = transportFootprint;
      }

      return getUpdatedState(state);
    }
    case GET_AIR_QUALITY: {
      return state;
    }
  }
  return state;
}

function removeEntry(entry: ItemFootprint){
    entryStore.entries.forEach( (item, index) => {
        if(item === entry) entryStore.entries.splice(index,1);
    });
}

function getUpdatedState(
  state: StoreState
): StoreState {

  let allEntries: ItemFootprint[] = [];
  entryStore.entries.forEach(function (entry) {
      allEntries.push(entry);
  });
  if (entryStore.houseSizeConfig)
    allEntries.push(entryStore.houseSizeConfig);
  if (entryStore.houseEnergyConfig)
    allEntries.push(entryStore.houseEnergyConfig);
  if (entryStore.transportConfig)
    allEntries.push(entryStore.transportConfig);

  let earthsNumber = getNumberOfEarths(allEntries) * 100;
  console.log(earthsNumber);
  let details = getDetailedNumberOfEarths(allEntries);
  details.Emission = details.Emission * 100;
  details.Land = details.Land * 100;
  details.Waste= details.Waste * 100;
  details.Water = details.Water * 100;
  let suggestions = getRecommendation(allEntries);

  console.log(suggestions);

  return {
    ...state,
    earthsNumber: earthsNumber,
    details: details,
    recommandations: suggestions
  };
}

