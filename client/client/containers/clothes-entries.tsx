import { ClothesEntries } from '../components/clothes-entries';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Clothe from '../models/clothe';

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
        addClotheEntry: (clothe: Clothe) => dispatch(actions.addClothe(clothe)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClothesEntries);