import { ObjectsEntries } from '../components/objects-entries';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Object from '../models/usualObjects'

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
        addObjectEntry: (object: Object) => dispatch(actions.addUsualObjects(object))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectsEntries);