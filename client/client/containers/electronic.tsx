import { ElectronicEntries } from '../components/electronic-entries';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Electronic from '../models/electronic';

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
        addElectronicEntry: (electronic: Electronic) => dispatch(actions.addElectronic(electronic))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ElectronicEntries);