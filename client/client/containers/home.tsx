import { Home } from "../components/home";
import * as actions from "../actions/";
import { StoreState } from "../types/index";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";

export function mapStateToProps(storeState: StoreState) {
  return {
    enthusiasmLevel: storeState.enthusiasmLevel,
    name: storeState.languageName,
    earthsNumber: storeState.earthsNumber,
    details: storeState.details,
    suggestions: storeState.recommandations,
    airQuality: storeState.airQuality
  };
}

// export function mapDispatchToProps(
//   dispatch: Dispatch<actions.EnthusiasmAction>
// ) {
//   return {
//     getEarthsNumber: () => dispatch(actions.getFootprint()),
//     getAirQuality: () => dispatch(actions.getAirQUality())
//   };
// }

export function mapDispatchToProps(dispatch: ThunkDispatch<any, any, any>) {
  return{
    getAirQuality: () => dispatch(actions.getAirQUality())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
