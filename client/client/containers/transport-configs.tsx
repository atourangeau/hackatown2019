import { TransportConfigs } from '../components/transport-configs';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Transport from "../models/transport";

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
        configureTransport: (transport: Transport) => dispatch(actions.configureTransport(transport)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TransportConfigs);