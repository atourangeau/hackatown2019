import { HabitationConfigs } from '../components/habitation-configs';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Habitation from "../models/habitation";

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
        configureHabitation: (habitation: Habitation) => dispatch(actions.configureHabitation(habitation)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HabitationConfigs);