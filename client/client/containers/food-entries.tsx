import { FoodEntries } from '../components/food-entries';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Food from '../models/food';

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
        addFoodEntry: (food: Food) => dispatch(actions.addFood(food))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodEntries);