import { Entries } from '../components/entries';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export function mapStateToProps( storeState: StoreState) {
    return {
    }
}

export function mapDispatchToProps(dispatch: Dispatch<actions.EnthusiasmAction>) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Entries);