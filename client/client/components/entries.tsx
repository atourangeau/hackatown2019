import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Button } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import { commonStyle } from "./home";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
}

export interface State {}

export class Entries extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

    static navigationOptions = {
        title: "Ajout d'items",
        headerStyle: {
            backgroundColor: '#00420c',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

  dummy = () => {
    this.props.navigation.navigate('ClothesEntries')
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column", alignItems:'center', backgroundColor: "#001e05", padding: "10%"}}>
        <View style={{ flex: 1, flexDirection: "row", padding: 12, paddingBottom: 0}}>
          <Button
            icon={{ name: "md-shirt", size: 50, type: "ionicon" }}
            onPress={() => this.props.navigation.navigate('ClothesEntries')}
            title=""
            buttonStyle={commonStyle.bigButtonStyle}
          />
          <Button
            icon={{ name: "coffee", size: 50, type: "font-awesome" }}
            onPress={() => this.props.navigation.navigate('FoodEntries')}
            title=""
            buttonStyle={commonStyle.bigButtonStyle}
          />
        </View>
        <View style={{ flex: 1, flexDirection: "row", padding: 12, paddingTop: 0}}>
          <Button
            icon={{ name: "smartphone", size: 50, type: "material-icons" }}
            onPress={() => this.props.navigation.navigate('ElectronicEntries')}
            title=""
            buttonStyle={commonStyle.bigButtonStyle}
          />
          <Button
            icon={{ name: "book", size: 50, type: "font-awesome" }}
            onPress={() => this.props.navigation.navigate('ObjectEntries')}
            title=""
            buttonStyle={commonStyle.bigButtonStyle}
          />
        </View>
          <View style={{ flex: 0, flexDirection: "row"}}>
              <Button
                  icon={{ name: "car", size: 25, type: "font-awesome" }}
                  onPress={() => this.props.navigation.navigate('TransportEntries')}
                  title=""
                  buttonStyle={{
                      backgroundColor: "#00420c",
                      width: "100%",
                      borderColor: "black",
                      borderWidth: 2,
                      borderRadius: 5
                  }}
              />
          </View>
      </View>
    );
  }
}
