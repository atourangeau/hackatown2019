import React from "react";
import { View, Text, Image, StyleSheet, Picker, TextInput } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Button, CheckBox } from "react-native-elements";
import Clothe from "../models/clothe";
import { commonStyle } from "./home";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
  addClotheEntry: (clothe: Clothe) => void;
}

interface State {
  clotheType: ClothesTypeEnum;
  occurence: string;
  isBio: boolean;
  isReused: boolean;
}

export enum ClothesTypeEnum {
  Default = "default",
  Shoes = "Fabric_Shoes",
  Jeans = "Fabric_Jean",
  TShirtLin = "Fabric_Lin",
  TShirtCotton = "Fabric_Cotton",
  Coat = "Fabric_Coat"
}

export class ClothesEntries extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      clotheType: ClothesTypeEnum.Default,
      occurence: "0",
      isBio: false,
      isReused: false
    };
  }

  static navigationOptions = {
    title: "Vêtements",
    headerStyle: {
      backgroundColor: "#00420c"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    }
  };

  addEntry = () => {
    let clothe = {
      clotheType: this.state.clotheType,
      occurence: parseInt(this.state.occurence, 10),
      isBio: this.state.isBio,
      isReused: this.state.isReused
    };

    this.props.addClotheEntry(clothe);
    this.resetState();
  };

  resetState = () => {
    this.setState({
      clotheType: ClothesTypeEnum.Default,
      occurence: "0",
      isBio: false,
      isReused: false
    });
  };

  isAddButtonDisable = () => {
      return !(this.state.clotheType !== ClothesTypeEnum.Default && parseInt(this.state.occurence, 10) > 0);
  };

  render() {
    return (
      <View style={{ padding: 10, backgroundColor: "#001e05", height: "100%" }}>
        <View style={{ borderColor: "black", borderWidth: 2, paddingLeft: 5 }}>
          <Text style={{ color: "white" }}>Vêtement acheté</Text>
          <Picker
            selectedValue={this.state.clotheType}
            onValueChange={itemValue =>
              this.setState({ ...this.state, clotheType: itemValue })
            }
          >
            <Picker.Item
              label="Defaut"
              value={ClothesTypeEnum.Default}
              color="gray"
            />
            <Picker.Item
              label="Soulier"
              value={ClothesTypeEnum.Shoes}
              color="gray"
            />
            <Picker.Item
              label="Jeans"
              value={ClothesTypeEnum.Jeans}
              color="gray"
            />
            <Picker.Item
              label="T-Shirt en Lin"
              value={ClothesTypeEnum.TShirtLin}
              color="gray"
            />
            <Picker.Item
              label="T-Shirt en Cotton"
              value={ClothesTypeEnum.TShirtCotton}
              color="gray"
            />
            <Picker.Item
              label="Manteau"
              value={ClothesTypeEnum.Coat}
              color="gray"
            />
          </Picker>
        </View>

        <View
          style={{
            borderColor: "black",
            borderWidth: 2,
            marginTop: 5,
            paddingLeft: 5
          }}
        >
          <Text style={{ color: "white" }}>Quantité</Text>
          <TextInput
            value={this.state.occurence}
            keyboardType="numeric"
            onChangeText={text =>
              this.setState({ ...this.state, occurence: text })
            }
            style={{ color: "grey" }}
            returnKeyType="done"
          />
        </View>

        <CheckBox
          center
          title="Tissu Biologique"
          checked={this.state.isBio}
          onPress={() =>
            this.setState({ ...this.state, isBio: !this.state.isBio })
          }
        />
        <CheckBox
          center
          title="Seconde main"
          checked={this.state.isReused}
          onPress={() =>
            this.setState({ ...this.state, isReused: !this.state.isReused })
          }
        />

        <Button
          title="Ajouter"
          onPress={this.addEntry}
          disabled={this.isAddButtonDisable()}
          buttonStyle={commonStyle.buttonStyle}
          disabledStyle={commonStyle.disabledButtonStyle}
        />
      </View>
    );
  }
}
