import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import { createStore } from "redux";
import { earthReducer, initialState } from "../reducers/index";
import { StoreState } from "../types/index";
import { NavigationScreenProp } from "react-navigation";
import { Button, Icon } from "react-native-elements";
import { Footprint } from "../services/FootprintService";
import { recommendation } from "../services/AnnualFootprintService";
import { string } from "prop-types";
import AirQuality, { AirStatusEnum } from "../models/air-quality";

export interface Props {
  name: string;
  enthusiasmLevel?: number;
  earthsNumber: number;
  details: Footprint;
  suggestions: Map<string, recommendation>;
  navigation: NavigationScreenProp<any, any>;
  airQuality: AirQuality;
  getAirQuality: () => Promise<AirQuality>;
}

export enum stateEnum {
  Default = 0,
  ShowDetails = 1,
  ShowSuggestions = 2
}

export interface State {
  currentState: stateEnum;
  airQuality: AirQuality;
}

export const commonStyle = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#00420c",
    borderColor: "black",
    borderWidth: 2,
    borderRadius: 5
  },
  disabledButtonStyle: {
    backgroundColor: "rgba(255, 255, 255, 0.3)",
    borderColor: "#242424",
    borderWidth: 2,
    borderRadius: 5
  },
  bigButtonStyle: {
    backgroundColor: "#00420c",
    borderColor: "black",
    width: "96%",
    height: "75%",
    borderWidth: 2,
    borderRadius: 5
  }
});

export class Home extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      currentState: stateEnum.Default,
      airQuality:{value: 0, status: AirStatusEnum.Unknown}
    };
  }

  componentDidMount() {
    this.props.getAirQuality().then((value: AirQuality) => this.setState({...this.state, airQuality: value}));
  }

  static navigationOptions = {
    title: "Tableau de bord",
    headerStyle: {
      backgroundColor: "#00420c"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    }
  };

  addUserAction = () => {
    this.props.navigation.navigate("Entries");
  };

  goToConfigs = () => {
    this.props.navigation.navigate("TransportConfigs");
  };

  nextState = () => {
    if (this.state.currentState === stateEnum.Default) {
      this.setState({ ...this.state, currentState: stateEnum.ShowDetails });
    } else if (this.state.currentState === stateEnum.ShowDetails) {
      this.setState({ ...this.state, currentState: stateEnum.ShowSuggestions });
    } else {
      this.setState({ ...this.state, currentState: stateEnum.Default });
    }
  };

  renderSubView = () => {
    if (this.state.currentState === stateEnum.Default) {
      return <View />;
    }
    if (this.state.currentState === stateEnum.ShowDetails) {
      return (
        <View>
          <Text style={{ color: "white" }}>Détail: </Text>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white" }}>Émission: </Text>
            <Text style={{ color: "white" }}>
              {this.props.details.Emission}%
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white" }}>Terrain: </Text>
            <Text style={{ color: "white" }}>{this.props.details.Land}%</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white" }}>Déchets: </Text>
            <Text style={{ color: "white" }}>{this.props.details.Waste}%</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "white" }}>Eau: </Text>
            <Text style={{ color: "white" }}>{this.props.details.Water}%</Text>
          </View>
        </View>
      );
    }
    if (this.state.currentState === stateEnum.ShowSuggestions) {
      if (this.props.suggestions !== undefined) {
        let array = Array.from(this.props.suggestions.keys());
        return (
          <View>
            <Text style={{ color: "white" }}>Suggestions: </Text>
            {array.map((value:string) => { 
              let recommandation = this.props.suggestions.get(value);
              if(recommandation !== undefined){
                return this.renderRecommendation(value, recommandation)
              }
            }) }
          </View>
        );
      }
      else{
        return ( <Text style={{ color: "white" }}>Aucune Suggestion</Text>)
      }
    }
  };

  getEmissionString(value: string) : string {
      switch (value) {
          case "Emission":
              return "vos émissions de CO2";
          case "Water":
              return "votre consommation d'eau";
          case "Waste":
              return "votre génération de déchets";
          case "Land":
              return "votre utilisation des resources"
      }
      return "RIP you found a bug :)";
  }

    getItemTypeString(value: string) : string {
        switch (value) {
            case "Fabric":
                return "les vêtements. Pensez réduire/réutiliser/recycler. Les vêtements en cotton bio sont aussi une solution à faible impact.";
            case "Food":
                return "la nourriture. Essayez de réduire la consommation de boeuf et d'acheter bio/local.";
            case "Housing":
                return "votre habitation. Pensez cohabitation, réduction des espaces perdus, amélioration de l'étanchéité thermique.";
            case "Object":
                return "votre consommation d'objets. Écoutez le documentaire 'Minimalism'.";
            case "Transport":
                return "vos transports. Évitez l'avion et favorisez le transport en commun. Se rapprocher du travail est aussi une bonne solution.";
        }
        return "RIP you found a bug :)";
    }

  renderRecommendation = (value: string, recommandation: recommendation) => {
    console.log(value, recommandation)
    return (
      <Text style={{ color: "white", fontSize: 8}}>{recommandation.AnnualPercentage}% de {this.getEmissionString(value)} est causée par {this.getItemTypeString(recommandation.ItemType)} </Text>
    );
  };

  render() {
    let statusColor = 'green';
    if(this.state.airQuality.status === AirStatusEnum.Acceptable) statusColor = 'yellow';
    if(this.state.airQuality.status === AirStatusEnum.Bad) statusColor = 'red';

    return (
      <View style={{ flex: 1, paddingTop: 10, backgroundColor: "#001e05" }}>
      <View style={{flexDirection:'row'}}>
        <Text style={{color:'white', fontWeight: "bold"}}>Qualité de l'air: </Text>
        <Text style={{color:statusColor}}>{this.state.airQuality.status}</Text>
      </View>
        <View style={{ flex: 1, alignSelf: "center", paddingTop: 20 }}>
          <TouchableHighlight onPress={this.nextState}>
            <View>
              <Image
                style={{ width: 300, height: 300 }}
                source={require("../assets/worldwide.png")}
              />
              <Text
                style={{
                  position: "absolute",
                  flex: 1,
                  alignSelf: "center",
                  paddingTop: 90,
                  fontSize: 100
                }}
              >
                {this.props.earthsNumber}%
              </Text>
            </View>
          </TouchableHighlight>
        </View>
        <View style={{ alignSelf: "center", marginTop: 240 }}>
          {this.renderSubView()}
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "flex-end",
            marginBottom: 36,
            marginLeft: 20
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Button
              icon={{ name: "add-circle", size: 30, color: "white" }}
              title=""
              onPress={this.addUserAction}
              buttonStyle={{
                backgroundColor: "#00420c",
                width: "100%",
                borderColor: "black",
                borderWidth: 2,
                borderRadius: 5
              }}
            />
            <Button
              icon={{ name: "settings", size: 30, color: "white" }}
              title=""
              onPress={this.goToConfigs}
              buttonStyle={commonStyle.buttonStyle}
            />
          </View>
        </View>
      </View>
    );
  }
}
