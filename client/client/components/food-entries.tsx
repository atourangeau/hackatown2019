import React from "react";
import { View, Text, Image, StyleSheet, Picker, TextInput } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Button, CheckBox } from "react-native-elements";
import { commonStyle } from "./home";
import food from "../models/food";
import Food from "../models/food";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
  addFoodEntry: (food: food) => void;
}

export interface State {
  foodType: FoodTypeEnum;
  portion: string;
  isBio: boolean;
}

export enum FoodTypeEnum {
  Default = "default",
  WhiteMeat = "Food_WhiteMeat",
  RedMeatH = "Food_RedMeatH",
  RedMeatL = "Food_RedMeatL",
  Fish = "Food_RedMeatL",
  Dry = "Food_Dry",
  Other = "Food_Other"
}

export class FoodEntries extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      foodType: FoodTypeEnum.Default,
      portion: "0",
      isBio: false
    };
  }
  static navigationOptions = {
    title: "Nourriture",
    headerStyle: {
      backgroundColor: "#00420c"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold"
    }
  };

  addEntry = () => {
    let food: Food = {
      foodType: this.state.foodType,
      portion: parseInt(this.state.portion, 10),
      isBio: this.state.isBio
    };

    this.props.addFoodEntry(food);
    this.resetState();
  };

  resetState = () => {
    this.setState({
      foodType: FoodTypeEnum.Default,
      portion: "0",
      isBio: false
    });
  };

  isAddButtonDisable = () => {
    return !(
      this.state.foodType !== FoodTypeEnum.Default &&
      parseInt(this.state.portion, 10) > 0
    );
  };

  render() {
    console.log(this.state);
    return (
      <View style={{ padding: 10, backgroundColor: "#001e05", height: "100%" }}>
        <View style={{ borderColor: "black", borderWidth: 2, paddingLeft: 5 }}>
          <Text style={{ color: "white" }}>Nourriture achetée</Text>
          <Picker
            selectedValue={this.state.foodType}
            onValueChange={itemValue =>
              this.setState({ ...this.state, foodType: itemValue })
            }
          >
            <Picker.Item
              label="Defaut"
              value={FoodTypeEnum.Default}
              color="gray"
            />
            <Picker.Item
              label="Volaille"
              value={FoodTypeEnum.WhiteMeat}
              color="gray"
            />
            <Picker.Item
              label="Autre Viande Rouge"
              value={FoodTypeEnum.RedMeatL}
              color="gray"
            />
            <Picker.Item
              label="Boeuf"
              value={FoodTypeEnum.RedMeatH}
              color="gray"
            />
            <Picker.Item
              label="Poisson"
              value={FoodTypeEnum.Fish}
              color="gray"
            />
            <Picker.Item
              label="Nourriture Seche"
              value={FoodTypeEnum.Dry}
              color="gray"
            />
            <Picker.Item
              label="Autre"
              value={FoodTypeEnum.Other}
              color="gray"
            />
          </Picker>
        </View>

        <View
          style={{
            borderColor: "black",
            borderWidth: 2,
            marginTop: 5,
            paddingLeft: 5
          }}
        >
          <Text style={{ color: "white" }}>Quantité</Text>
          <TextInput
            value={this.state.portion}
            keyboardType="numeric"
            onChangeText={text =>
              this.setState({ ...this.state, portion: text })
            }
            style={{ color: "grey" }}
            returnKeyType="done"
          />
        </View>

        <CheckBox
          center
          title="Biologique"
          checked={this.state.isBio}
          onPress={() =>
            this.setState({ ...this.state, isBio: !this.state.isBio })
          }
        />

        <Button
          title="Ajouter"
          onPress={this.addEntry}
          disabled={this.isAddButtonDisable()}
          buttonStyle={commonStyle.buttonStyle}
          disabledStyle={commonStyle.disabledButtonStyle}
        />
      </View>
    );
  }
}
