import React from "react";
import { View, Text, Image, StyleSheet, Picker, TextInput } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Button, CheckBox } from "react-native-elements";
import {commonStyle} from "./home";
import electronic from "../models/electronic";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
  addElectronicEntry:(electronic: electronic) => void;
}

export interface State {
  ElectronicType: ElectronicTypeEnum;
  occurence: string;
  isReused: boolean;
}
export enum ElectronicTypeEnum {
    Default = "default",
    Phone = "Object_Phone",
    Desktop = "Object_Desktop",
    Laptop = "Object_Laptop",
    Monitor = "Object_Monitor"
  }

export class ElectronicEntries extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      ElectronicType: ElectronicTypeEnum.Default, 
      occurence: "0",
      isReused: false
    };
  }
  static navigationOptions = {
    title: 'Electronique',
    headerStyle: {
        backgroundColor: '#00420c',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontWeight: 'bold',
    },
};

addEntry = () => {
  let electronic: electronic = {
    electronicType: this.state.ElectronicType,
    occurence: parseInt(this.state.occurence, 10),
    isReused: this.state.isReused
  };

  this.props.addElectronicEntry(electronic);
  this.resetState();
};

resetState = () => {
  this.setState({
    ElectronicType: ElectronicTypeEnum.Default,
    occurence: "0",
    isReused: false
  });
};

isAddButtonDisable = () => {
  return !(this.state.ElectronicType !== ElectronicTypeEnum.Default && parseInt(this.state.occurence, 10) > 0);
}

render() {
  console.log(this.state);
  return (
    <View style={{ padding: 10 , backgroundColor: "#001e05", height: "100%"}}>
      <View style={{ borderColor: "black", borderWidth: 2, paddingLeft: 5 }}>
        <Text style={{ color: "white" }}>Electronique acheté</Text>
        <Picker
          selectedValue={this.state.ElectronicType}
          onValueChange={itemValue =>
            this.setState({ ...this.state, ElectronicType: itemValue })
          }
        >
          <Picker.Item label="Defaut" value={ElectronicTypeEnum.Default} color="gray" />
          <Picker.Item label="Desktop" value={ElectronicTypeEnum.Desktop} color="gray" />
          <Picker.Item label="Laptop" value={ElectronicTypeEnum.Laptop} color="gray" />
          <Picker.Item label="Écran/TV" value={ElectronicTypeEnum.Monitor} color="gray" />
          <Picker.Item label="Téléphone" value={ElectronicTypeEnum.Phone} color="gray" />
        </Picker>
      </View>

      <View
        style={{
          borderColor: "black",
          borderWidth: 2,
          marginTop: 5,
          paddingLeft: 5
        }}
      >
        <Text style={{ color: "white" }}>Quantité</Text>
        <TextInput
          value={this.state.occurence}
          keyboardType="numeric"
          onChangeText={text =>
            this.setState({ ...this.state, occurence: text })
          }
          style={{ color: "grey" }}
          returnKeyType='done'
        />
      </View>

    
      <CheckBox
        center
        title="Seconde main"
        checked={this.state.isReused}
        onPress={() =>
          this.setState({ ...this.state, isReused: !this.state.isReused })
        }
      />

      <Button
        title="Ajouter"
        onPress={this.addEntry}
        disabled={this.isAddButtonDisable()}
        buttonStyle={commonStyle.buttonStyle}
        disabledStyle={commonStyle.disabledButtonStyle}
      />
    </View>
  );
}
}
