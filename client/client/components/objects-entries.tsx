import React from "react";
import { View, Text, Image, StyleSheet, Picker, TextInput } from "react-native";
import { NavigationScreenProp } from "react-navigation";
import { Button, CheckBox } from "react-native-elements";
import {commonStyle} from "./home";
import usualObjects from "../models/usualObjects";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
   addObjectEntry:(object: object) => void;
}

export interface State {
  ObjectType: ObjectTypeEnum;
  occurence: string;
  isReused: boolean;
}
export enum ObjectTypeEnum {
  Default = "default",
  Furniture = "Housing_Furniture",
  Electro = "Housing_Electro",
  Recyclable = "Object_Recyclable",
  Trash = "Object_Trash"
}
export class ObjectsEntries extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      ObjectType: ObjectTypeEnum.Default, 
      occurence: "0",
      isReused: false
    };
  }
  static navigationOptions = {
    title: 'Divers',
    headerStyle: {
        backgroundColor: '#00420c',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontWeight: 'bold',
    },
  };  
    addEntry = () => {
      let object: usualObjects = {
        objectType: this.state.ObjectType,
        occurence: parseInt(this.state.occurence, 10),
        isReused: this.state.isReused
      };
    
       this.props.addObjectEntry(object);
       this.resetState();
    };

    resetState = () => {
      this.setState({
        ObjectType: ObjectTypeEnum.Default,
        occurence: "0",
        isReused: false
      });
    };

    isAddButtonDisable = () => {
      return !(this.state.ObjectType !== ObjectTypeEnum.Default && parseInt(this.state.occurence, 10) > 0);
    }

    render() {
      console.log(this.state);
      return (
        <View style={{ padding: 10 , backgroundColor: "#001e05", height: "100%"}}>
          <View style={{ borderColor: "black", borderWidth: 2, paddingLeft: 5 }}>
            <Text style={{ color: "white" }}>Divers acheté</Text>
            <Picker
              selectedValue={this.state.ObjectType}
              onValueChange={itemValue =>
                this.setState({ ...this.state, ObjectType: itemValue })
              }
            >
              <Picker.Item label="Defaut" value={ObjectTypeEnum.Default} color="gray" />
              <Picker.Item label="Electromenager" value={ObjectTypeEnum.Electro} color="gray" />
              <Picker.Item label="Meuble" value={ObjectTypeEnum.Furniture} color="gray" />
              <Picker.Item label="Objet Recyclable" value={ObjectTypeEnum.Recyclable} color="gray" />
              <Picker.Item label="Objet Non Recyclable" value={ObjectTypeEnum.Trash} color="gray" />
            </Picker>
          </View>
    
          <View
            style={{
              borderColor: "black",
              borderWidth: 2,
              marginTop: 5,
              paddingLeft: 5
            }}
          >
            <Text style={{ color: "white" }}>Quantité</Text>
            <TextInput
              value={this.state.occurence}
              keyboardType="numeric"
              onChangeText={text =>
                this.setState({ ...this.state, occurence: text })
              }
              style={{ color: "grey" }}
              returnKeyType='done'
            />
          </View>
    
        
          <CheckBox
            center
            title="Seconde main"
            checked={this.state.isReused}
            onPress={() =>
              this.setState({ ...this.state, isReused: !this.state.isReused })
            }
          />
    
          <Button
            title="Ajouter"
            onPress={this.addEntry}
            disabled={this.isAddButtonDisable()}
            buttonStyle={commonStyle.buttonStyle}
            disabledStyle={commonStyle.disabledButtonStyle}
          />
        </View>
      );
    }
}
