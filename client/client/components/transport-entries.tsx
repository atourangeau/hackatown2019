import React from "react";
import { View, Text, Picker, TextInput } from "react-native";
import { Button } from "react-native-elements";
import { NavigationScreenProp } from "react-navigation";
import {commonStyle} from "./home";
import Transport from "../models/transport";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
  addTransport: (transport: Transport) => void;
}

interface State {
    transportType: TransportTypeEnum;
    hoursOfUse: string;
    daysPerWeek: string;
}

export enum TransportTypeEnum {
    Default = "default",
    Walk = "Transport_Manual",
    Plane = "Transport_Plane",
    CommuteElec = "Transport_CommuteElec",
    CommuteGas = "Transport_CommuteGas",
    Electric = "Transport_Electric",
    Hybrid = "Transport_Hybrid",
    GasLight = "Transport_LightGaz",
    GasHeavy = "Transport_HeavyGaz"
}

export class TransportEntries extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

      this.state = {
          transportType: TransportTypeEnum.Default,
          hoursOfUse: "0",
          daysPerWeek: "0"
      };
  }

    static navigationOptions = {
        title: 'Transports courants',
        headerStyle: {
            backgroundColor: '#00420c',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    submit = () => {
        let transport = {
            transportType: this.state.transportType,
            hoursOfUse: parseInt(this.state.hoursOfUse, 10),
            daysPerWeek: 1
        }
        this.props.addTransport(transport);
        this.resetState();
    };

    resetState = () => {
        this.setState({
            transportType: TransportTypeEnum.Default,
            hoursOfUse: "0",
            daysPerWeek: "0"
        });
    };

    isButtonDisable = () => {
        return !(this.state.transportType !== TransportTypeEnum.Default && parseInt(this.state.hoursOfUse, 10) > 0);
    };

  render() {
    return (
        <View style={{ padding: 10 , backgroundColor: "#001e05", height: "100%"}}>
            <View style={{ borderColor: "black", borderWidth: 2, paddingLeft: 5 }}>
                <Text style={{ color: "white" }}>Transport utilisé</Text>
                <Picker
                    selectedValue={this.state.transportType}
                    onValueChange={itemValue =>
                        this.setState({ ...this.state, transportType: itemValue })
                    }
                >
                    <Picker.Item label="Defaut" value={TransportTypeEnum.Default} color="gray" />
                    <Picker.Item label="Marche/Vélo" value={TransportTypeEnum.Walk} color="gray" />
                    <Picker.Item label="Metro" value={TransportTypeEnum.CommuteElec} color="gray" />
                    <Picker.Item label="Bus" value={TransportTypeEnum.CommuteGas} color="gray" />
                    <Picker.Item label="Voiture électrique" value={TransportTypeEnum.Electric} color="gray" />
                    <Picker.Item label="Voiture hybride" value={TransportTypeEnum.Hybrid} color="gray" />
                    <Picker.Item label="Voiture compacte" value={TransportTypeEnum.GasLight} color="gray" />
                    <Picker.Item label="Voiture haute performance" value={TransportTypeEnum.GasHeavy} color="gray" />
                    <Picker.Item label="Gros pickup m'en calliss" value={TransportTypeEnum.GasHeavy} color="gray" />
                    <Picker.Item label="Avion" value={TransportTypeEnum.Plane} color="gray" />
                </Picker>
            </View>

            <View
                style={{
                    borderColor: "black",
                    borderWidth: 2,
                    marginTop: 5,
                    paddingLeft: 5
                }}
            >
                <Text style={{ color: "white" }}>Heures d'utilisation</Text>
                <TextInput
                    value={this.state.hoursOfUse}
                    keyboardType="numeric"
                    onChangeText={text =>
                        this.setState({ ...this.state, hoursOfUse: text })
                    }
                    style={{ color: "grey" }}
                />
            </View>

            <Button
                title="Ajouter"
                onPress={this.submit}
                disabled={this.isButtonDisable()}
                buttonStyle={commonStyle.buttonStyle}
                disabledStyle={commonStyle.disabledButtonStyle}
            />
        </View>
    );
  }
}
