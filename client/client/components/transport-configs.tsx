import React from "react";
import { View, Text, Picker, TextInput } from "react-native";
import { Button } from "react-native-elements";
import { NavigationScreenProp } from "react-navigation";
import {commonStyle} from "./home";
import Transport from "../models/transport";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
  configureTransport: (transport: Transport) => void;
}

interface State {
    transportType: TransportTypeEnum;
    hoursOfUse: string;
    daysPerWeek: string;
}

export enum TransportTypeEnum {
    Default = "default",
    Walk = "Transport_Manual",
    Plane = "Transport_Plane",
    CommuteElec = "Transport_CommuteElec",
    CommuteGas = "Transport_CommuteGas",
    Electric = "Transport_Electric",
    Hybrid = "Transport_Hybrid",
    GasLight = "Transport_LightGaz",
    GasHeavy = "Transport_HeavyGaz"
}

export class TransportConfigs extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

      this.state = {
          transportType: TransportTypeEnum.Default,
          hoursOfUse: "0",
          daysPerWeek: "0"
      };
  }

    static navigationOptions = {
        title: 'Transports courants',
        headerStyle: {
            backgroundColor: '#00420c',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    nextPage = () => {
        let transport = {
            transportType: this.state.transportType,
            hoursOfUse: parseInt(this.state.hoursOfUse, 10),
            daysPerWeek: parseInt(this.state.daysPerWeek, 10)
        }
        this.props.configureTransport(transport);
        this.props.navigation.navigate('HabitationConfigs');
    };

    isButtonDisable = () => {
        return !(this.state.transportType !== TransportTypeEnum.Default
            && parseInt(this.state.hoursOfUse, 10) > 0 && parseInt(this.state.hoursOfUse, 10) <= 24
            && parseInt(this.state.daysPerWeek, 10) > 0 && parseInt(this.state.daysPerWeek, 10) <= 7);
    };

  render() {
    return (
        <View style={{ padding: 10 , backgroundColor: "#001e05", height: "100%"}}>
            <View style={{ borderColor: "black", borderWidth: 2, paddingLeft: 5 }}>
                <Text style={{ color: "white" }}>Type de transport le plus utilisé</Text>
                <Picker
                    selectedValue={this.state.transportType}
                    onValueChange={itemValue =>
                        this.setState({ ...this.state, transportType: itemValue })
                    }
                >
                    <Picker.Item label="Defaut" value={TransportTypeEnum.Default} color="gray" />
                    <Picker.Item label="Marche/Vélo" value={TransportTypeEnum.Walk} color="gray" />
                    <Picker.Item label="Metro" value={TransportTypeEnum.CommuteElec} color="gray" />
                    <Picker.Item label="Bus" value={TransportTypeEnum.CommuteGas} color="gray" />
                    <Picker.Item label="Voiture électrique" value={TransportTypeEnum.Electric} color="gray" />
                    <Picker.Item label="Voiture hybride" value={TransportTypeEnum.Hybrid} color="gray" />
                    <Picker.Item label="Voiture compacte" value={TransportTypeEnum.GasLight} color="gray" />
                    <Picker.Item label="Voiture haute performance" value={TransportTypeEnum.GasHeavy} color="gray" />
                    <Picker.Item label="Gros pickup m'en calliss" value={TransportTypeEnum.GasHeavy} color="gray" />
                </Picker>
            </View>

            <View
                style={{
                    borderColor: "black",
                    borderWidth: 2,
                    marginTop: 5,
                    paddingLeft: 5
                }}
            >
                <Text style={{ color: "white" }}>Heures d'utilisation par jour</Text>
                <TextInput
                    value={this.state.hoursOfUse}
                    keyboardType="numeric"
                    onChangeText={text =>
                        this.setState({ ...this.state, hoursOfUse: text })
                    }
                    style={{ color: "grey" }}
                />

                <Text style={{ color: "white" }}>Jours d'utilisation par semaine</Text>
                <TextInput
                    value={this.state.daysPerWeek}
                    keyboardType="numeric"
                    onChangeText={text =>
                        this.setState({ ...this.state, daysPerWeek: text })
                    }
                    style={{ color: "grey" }}
                />
            </View>

            <Button
                title="Configuration habitation"
                onPress={this.nextPage}
                disabled={this.isButtonDisable()}
                buttonStyle={commonStyle.buttonStyle}
                disabledStyle={commonStyle.disabledButtonStyle}
            />
        </View>
    );
  }
}
