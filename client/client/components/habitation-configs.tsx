import React from "react";
import { View, Text, StyleSheet, TextInput } from "react-native";
import { Button } from "react-native-elements";
import { NavigationScreenProp } from "react-navigation";
import {commonStyle} from "./home";
import Habitation from "../models/habitation";

export interface Props {
  navigation: NavigationScreenProp<any, any>;
  configureHabitation: (habitation: Habitation) => void;
}

interface State {
    sizeInSquareFt: string;
    annualEnergyCost: string;
    numberOfHabitants: string;
}

export class HabitationConfigs extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
        sizeInSquareFt: "0",
        annualEnergyCost: "0",
        numberOfHabitants: "0"
    };
  }

    static navigationOptions = {
        title: 'Configuration habitation',
        headerStyle: {
            backgroundColor: '#00420c',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    finish = () => {
        let habitation = {
            sizeInSquareFt: parseInt(this.state.sizeInSquareFt, 10),
            annualEnergyCost: parseInt(this.state.annualEnergyCost, 10),
            numberOfHabitants: parseInt(this.state.numberOfHabitants, 10)
        }
        this.props.configureHabitation(habitation);
        this.props.navigation.navigate('Home');
    };

    isButtonDisable = () => {
        return !(parseInt(this.state.sizeInSquareFt, 10) > 0 && parseInt(this.state.annualEnergyCost, 10) > 0 && parseInt(this.state.numberOfHabitants, 10) > 0);
    };


  render() {
    return (
        <View style={{ padding: 10 , backgroundColor: "#001e05", height: "100%"}}>
            <View
                style={{
                    borderColor: "black",
                    borderWidth: 2,
                    marginTop: 5,
                    paddingLeft: 5
                }}
            >
                <Text style={{ color: "white" }}>Grandeur en pieds carrés de l'habitation</Text>
                <TextInput
                    value={this.state.sizeInSquareFt}
                    keyboardType="numeric"
                    onChangeText={text =>
                        this.setState({ ...this.state, sizeInSquareFt: text })
                    }
                    style={{ color: "grey" }}
                />

                <Text style={{ color: "white" }}>Coût d'hydro par année</Text>
                <TextInput
                    value={this.state.annualEnergyCost}
                    keyboardType="numeric"
                    onChangeText={text =>
                        this.setState({ ...this.state, annualEnergyCost: text })
                    }
                    style={{ color: "grey" }}
                />

                <Text style={{ color: "white" }}>Nombre d'habitants</Text>
                <TextInput
                    value={this.state.numberOfHabitants}
                    keyboardType="numeric"
                    onChangeText={text =>
                        this.setState({ ...this.state, numberOfHabitants: text })
                    }
                    style={{ color: "grey" }}
                />
            </View>

            <Button
                title="Configuration habitation"
                onPress={this.finish}
                disabled={this.isButtonDisable()}
                buttonStyle={commonStyle.buttonStyle}
                disabledStyle={commonStyle.disabledButtonStyle}
            />
        </View>
    );
  }
}
