import { Entry } from "../models/entry";
import Clothe from "../models/clothe";
import { ItemFootprint } from "../services/FootprintService";

class EntryStore {
    entries: ItemFootprint[] = [];
    houseSizeConfig: ItemFootprint|undefined;
    houseEnergyConfig: ItemFootprint|undefined;
    transportConfig: ItemFootprint|undefined;
}

export const entryStore = new EntryStore();