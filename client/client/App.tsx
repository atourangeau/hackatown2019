import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Home  from './containers/home';
import Entries from './containers/entries';
import ClothesEntries from './containers/clothes-entries';
import { createStore, applyMiddleware } from 'redux';
import { earthReducer, initialState } from './reducers/index';
import { StoreState } from './types/index';
import { Provider } from 'react-redux';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ObjectEntries from './containers/object-entries';
import FoodEntries from './containers/food-entries';
import Electronic from './containers/electronic';
import HabitationConfigs from './containers/habitation-configs'
import TransportConfigs from './containers/transport-configs'
import thunk from 'redux-thunk';
import Test from './containers/home'
import TransportEntries from "./containers/transport-entries";
const store = createStore<StoreState, any, any, any>(earthReducer, initialState, applyMiddleware(thunk));

const RootStack = createStackNavigator(
  {
    Home: Test,
    Entries: Entries,
    ClothesEntries: ClothesEntries,
    ObjectEntries: ObjectEntries,
    FoodEntries: FoodEntries,
    ElectronicEntries: Electronic,
    HabitationConfigs: HabitationConfigs,
    TransportConfigs: TransportConfigs,
    TransportEntries: TransportEntries
  },
  {
    initialRouteName:'Home',
      cardStyle: {
          backgroundColor: 'rgba(0,0,0,0.5)',
      }
  });

  const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
