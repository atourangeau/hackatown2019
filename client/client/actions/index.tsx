import * as constants from "../constants/index";
import Clothe from "../models/clothe";
import Electronic from "../models/electronic";
import Food from "../models/food";
import UsualObjects from "../models/usualObjects";
import Habitation from "../models/habitation";
import Transport from "../models/transport";
import { ThunkAction } from "redux-thunk";
import AirQuality from "../models/air-quality";
import { Action } from "redux";
import { getAirQuality } from "../services/api";

export interface IncrementEnthusiam {
  type: constants.INCREMENT_ENTHUSIASM;
}

export interface DecrementEnthusiam {
  type: constants.DECREMENT_ENTHUSIASM;
}

export interface AddClothe {
  type: constants.ADD_CLOTHE;
  clothe: Clothe;
}

export interface AddElectronic {
  type: constants.ADD_ELECTRONIC;
  electronic: Electronic;
}

export interface AddFood {
  type: constants.ADD_FOOD;
  food: Food;
}

export interface AddUsualObjects {
  type: constants.ADD_USUAL_OBJECTS;
  usualObjects: UsualObjects;
}

export interface ConfigureHabitation {
    type: constants.CONFIGURE_HABITATION;
    habitation: Habitation;
}

export interface ConfigureTransport {
    type: constants.CONFIGURE_TRANSPORT;
    transport: Transport;
}

export interface AddTransport {
    type: constants.ADD_TRANSPORT;
    transport: Transport;
}

export interface GetFootprint {
  type: constants.GET_FOOTPRINT;
}

export interface GetAirQUality {
  type: constants.GET_AIR_QUALITY;
}

export type EnthusiasmAction =
  | IncrementEnthusiam
  | DecrementEnthusiam
  | AddClothe
  | AddElectronic
  | AddFood
  | AddUsualObjects
  | GetFootprint
  | ConfigureHabitation
  | ConfigureTransport
  | GetAirQUality
  | AddTransport;

export function addClothe(clothe: Clothe): AddClothe {
  return {
    type: constants.ADD_CLOTHE,
    clothe: clothe
  };
}

export function addElectronic(electronic: Electronic): AddElectronic {
  return {
    type: constants.ADD_ELECTRONIC,
    electronic: electronic
  };
}

export function addFood(food: Food): AddFood {
  return {
    type: constants.ADD_FOOD,
    food: food
  };
}

export function addUsualObjects(usualObject: UsualObjects): AddUsualObjects {
  return {
    type: constants.ADD_USUAL_OBJECTS,
    usualObjects: usualObject
  };
}

export function configureHabitation(habitation: Habitation): ConfigureHabitation {
    return {
        type: constants.CONFIGURE_HABITATION,
        habitation: habitation
    };
}

export function configureTransport(transport: Transport): ConfigureTransport {
    return {
        type: constants.CONFIGURE_TRANSPORT,
        transport: transport
    };
}

export function addTransport(transport: Transport): AddTransport {
    return {
        type: constants.ADD_TRANSPORT,
        transport: transport
    };
}

export function getFootprint(): GetFootprint {
  return {
    type: constants.GET_FOOTPRINT
  };
}

// export function getAirQUality(): GetAirQUality {
//   return {
//     type: constants.GET_AIR_QUALITY
//   };
// }

type MyRootState = {};
type MyExtraArg = undefined;
type MyThunkResult<R> = ThunkAction<R, MyRootState, MyExtraArg, Action>;

export function getAirQUality(): MyThunkResult<Promise<AirQuality>> {
  return (dispatch, getState) => Promise.resolve(getAirQuality())
}
