import AirQualityResponse from "./air-quality-response";

export default interface AirQualityContainer {
    $: AirQualityResponse;
}