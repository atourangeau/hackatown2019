import { ClothesTypeEnum } from "../components/clothes-entries";
import { Entry } from "./entry";

export default interface Clothe extends Entry {
    clotheType: ClothesTypeEnum,
    occurence: number,
    isBio: boolean,
    isReused: boolean
}