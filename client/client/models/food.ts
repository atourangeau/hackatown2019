import { FoodTypeEnum } from "../components/food-entries";

export default interface Food {
    foodType: FoodTypeEnum;
    portion: number,
    isBio: boolean
}