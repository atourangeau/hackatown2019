export enum AirStatusEnum {
    Unknown = "Inconnue",
    Good = "Bonne",
    Acceptable = "Acceptable",
    Bad = "Mauvaise"
}

export default interface AirQuality {
    value: number,
    status: AirStatusEnum
}