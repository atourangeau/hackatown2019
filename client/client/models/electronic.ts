import { ElectronicTypeEnum } from "../components/electronic-entries";
export default interface Electronic{
    electronicType: ElectronicTypeEnum;
    occurence: number;
    isReused: boolean;
}