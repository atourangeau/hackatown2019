import { ObjectTypeEnum } from "../components/objects-entries";
export default interface UsualObjects {
    objectType: ObjectTypeEnum;
    occurence: number;
    isReused: boolean;
}