import { Entry } from "./entry";

export default interface Habitation extends Entry {
    sizeInSquareFt: number,
    annualEnergyCost: number,
    numberOfHabitants: number
}