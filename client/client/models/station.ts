import AirQualityResponse from "./air-quality-response";
import Polluant from "./polluant";

export default interface Station {
    echantillon: Polluant[]
}