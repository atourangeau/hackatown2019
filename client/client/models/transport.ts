import { Entry } from "./entry";
import {TransportTypeEnum} from "../components/transport-configs";

export default interface Transport extends Entry {
    transportType: TransportTypeEnum,
    hoursOfUse: number,
    daysPerWeek: number
}