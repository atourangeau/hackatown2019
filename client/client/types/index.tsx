import { Footprint } from "../services/FootprintService";
import { recommendation } from "../services/AnnualFootprintService";
import AirQualityResponse from "../models/air-quality-response";
import AirQuality from "../models/air-quality";

export interface StoreState {
    languageName: string;
    enthusiasmLevel: number;
    earthsNumber: number;
    details: Footprint;
    recommandations: Map<string, recommendation>;
    airQuality: AirQuality;
}